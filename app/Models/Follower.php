<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    use HasFactory;
    protected $table = 'follower';
    protected $fillable = [
        'user_follower',
        'user_followed',
        'status_follower',
        'status_followed',
    ];

    public function user_follower(){
        return $this->belongsTo(User::class,'user_folllower');
    }
    public function user_followed(){
        return $this->belongsTo(User::class,'user_followed');
    }
}
