<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Question;
use App\Models\Category;

class LandingPageController extends Controller
{
    public function index()
    {
        $question = Question::all();
        $category = Category::all();
        
        $data = [
            'question' => $question, 
            'category' => $category
        ];
        return view('index',$data);
    }
}
