<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Question;
use App\Models\Category;
use App\Models\Follower;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $idUser = Auth::id();
        $user = User::where('id', $idUser)->first();
        $question = Question::all();
        $category = Category::all();
        $detailProfile = Profile::where('user_id', $idUser)->first();
        $follower = Follower::where('user_followed', $idUser)->first();
        // dd ($detailProfile);
        $data = [
            'question' => $question,
            'category' => $category,
            'detailProfile'  => $detailProfile,
            'follower'  => $follower,
            'user'  => $user
        ];
        return view('home',$data);
    }

    public function profile()
    {
        $idUser = Auth::id();
        $user = User::where('id', $idUser)->first();
        $detailProfile = Profile::where('user_id', $idUser)->first();
        $category = DB::table('category')->get();
        $follower = Follower::where('user_followed', $idUser)->first();
        $data = [
            'user'=>$user,
            'detailProfile'=>$detailProfile,
            'follower'  => $follower,
            'category' => $category
        ];
        return view('profile',$data);
    }
    public function storeProfile(Request $request)
    {
        $this->validate($request, [
             'address' => 'required',
             'biodata' => 'required',
             'birth' => 'required',
             'imageProfile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($image = $request->file('imageProfile')) {
            $destinationPath = 'img/profile/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage";
        }
        $profile = new Profile;
        $profile->address = $request->input('address');
        $profile->biodata = $request->input('biodata');
        $profile->image = $input['image'];
        $profile->user_id = Auth::id();
        $profile->birth = $request->input('birth');
        $profile->age = Carbon::parse($request->input('birth'))->diff(\Carbon\Carbon::now())->format('%y');
        $profile->created_at = now()->toDateTimeString();
        $profile->save();
        return redirect('/');
    }
}
