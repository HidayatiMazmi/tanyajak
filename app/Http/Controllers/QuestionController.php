<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
             'subject' => 'required',
             'content' => 'required',
             'category_id' => 'required',
             'imageQuestion' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($image = $request->file('imageQuestion')) {
            $destinationPath = 'img/question/';
            $questionImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $questionImage);
            $input['image'] = "$questionImage";
        }
        DB::table('question')->insert([
            'subject' => $request->input('subject'), 
            'content' => $request->input('content'), 
            'image' => $input['image'],
            'category_id' => $request->input('category_id'),
            'user_id' => Auth::id()
        ]);
 
        return redirect('/');
    }
    public function destroy($id){
        DB::table('question')->where('id','=',$id)->delete();
        return redirect('/question');
    }
}
