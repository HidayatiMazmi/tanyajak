<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Answer;
use App\Models\Question;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
             'reply' => 'required',
             'question_id' => 'required',
             'image_answer' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if ($image = $request->file('image_answer')) {
            $destinationPath = 'img/answer/';
            $answerImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $answerImage);
            $input['image'] = "$answerImage";
        }
        DB::table('answer')->insert([
            'content' => $request->input('reply'), 
            'image' => $input['image'],
            'question_id' => $request->input('question_id'),
            'user_id' => Auth::id()
        ]);
 
        return redirect('/');
    }
    public function index(){
        $answer = DB::table('answer')->get();
        return view('admin.answer.index',['answer' => $answer]);
    }
    public function create(){
        return view('admin.answer.create');
    }
    public function show($id){
        $answer = DB::table('answer')->find($id);
        return view('admin.answer.show',['answer' => $answer]);
    }
    public function edit($id){
        $answer = DB::table('answer')->find($id);
        return view('admin.answer.edit',['answer' => $answer]);
    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('answer')->where('id',$id)->update([
            'nama' => $request->input('nama'), 
            'umur' => $request->input('umur'), 
            'bio' => $request->input('bio') 
        ]);        
        return redirect('/answer');
    }
    public function destroy($id){
        DB::table('answer')->where('id','=',$id)->delete();
        return redirect('/answer');
    }
}
