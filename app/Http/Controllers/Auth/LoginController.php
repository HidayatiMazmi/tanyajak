<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show_login()
    {
        $question = DB::table('question')->get();
        $category = DB::table('category')->get();
        $data = [
            'question' => $question, 
            'category' => $category
        ];
        return view('auth.login',$data);
    }

    public function change_password(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required'
        ]);
        $current_password = $request->input('current_password');
        $new_password = $request->input('new_password');
        $confirm_new_password = $request->input('new_password');
        $user = User::findOrFail(Auth::id());
        if($current_password==$user->password){
            if($new_password==$confirm_new_password){
                $user = User::findOrFail(Auth::id());
                $user->password = bcrypt($new_password);
                $user->save();
                return redirect()->route('profile')->with('fail','Data Password Berhasil diubah');
            }else{
                return redirect()->route('profile')->with('fail','Data Password Tidak Sama Dengan Confirm');
            }
        }else{
            return redirect()->route('profile')->with('fail','Password Tidak Sesuai');
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}
