<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $category = DB::table('category')->get();
        return view('category.index',['category' => $category]);
    }
    public function create(){
        return view('category.create');
    }
    public function store(Request $request){
        $this->middleware('auth');
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $image = $request->input('image');
        if($image != null){
            $image = $request->input('image');
        }else{
            $image = "";
        }
        DB::table('category')->insert([
            'name' => $request->input('name'), 
            'description' => $request->input('description'), 
            'image' => $image
        ]);

        return redirect('/');
    }
    public function show($id){
        $category = DB::table('category')->find($id);
        return view('category.show',['category' => $category]);
    }
    public function edit($id){
        $category = DB::table('category')->find($id);
        return view('category.edit',['category' => $category]);
    }
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('category')->where('id',$id)->update([
            'nama' => $request->input('nama'), 
            'umur' => $request->input('umur'), 
            'bio' => $request->input('bio') 
        ]);        
        return redirect('/category');
    }
    public function destroy($id){
        DB::table('category')->where('id','=',$id)->delete();
        return redirect('/category');
    }
}
