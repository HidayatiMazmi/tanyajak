<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\LandingPageController;
use App\Http\controllers\HomeController;
use App\Http\controllers\QuestionController;
use App\Http\controllers\CategoryController;
use App\Http\controllers\AnswerController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[LandingPageController::class,'index']);

Auth::routes();

Route::namespace('Auth')->group(function () {
    Route::get('/login', [LoginController::class, 'show_login'])->name('login');
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
    Route::put('/change_password', [LoginController::class, 'change_password'])->name('change_password');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');

Route::post('/profile', [App\Http\Controllers\HomeController::class, 'storeProfile'])->name('store_profile');

Route::get('filemanager', [FileManagerController::class, 'index']);

Route::get('/question', [QuestionController::class, 'index'])->name('question');
Route::post('/question', [QuestionController::class, 'store'])->name('store_question');
Route::get('/question/{question_id}', [QuestionController::class, 'show'])->name('show_question');
Route::get('/question/{question_id}/edit', [QuestionController::class, 'edit'])->name('edit_question');
Route::put('/question/{question_id}', [QuestionController::class, 'update'])->name('update_question');
Route::delete('/question/{question_id}', [QuestionController::class, 'destroy'])->name('delete_question');

Route::get('/category', [CategoryController::class, 'index'])->name('category');
Route::post('/category', [CategoryController::class, 'store'])->name('store_category');
Route::get('/category/create', [CategoryController::class, 'create'])->name('create_category');
Route::get('/category/{category_id}', [CategoryController::class, 'show'])->name('show_category');
Route::get('/category/{category_id}/edit', [CategoryController::class, 'edit'])->name('edit_category');
Route::put('/category/{category_id}', [CategoryController::class, 'update'])->name('update_category');
Route::delete('/category/{category_id}', [CategoryController::class, 'destroy'])->name('delete_category');

Route::get('/answer', [AnswerController::class, 'index'])->name('answer');
Route::post('/answer', [AnswerController::class, 'store'])->name('store_answer');
Route::get('/answer/{answer_id}', [AnswerController::class, 'show'])->name('show_answer');
Route::get('/answer/{answer_id}/edit', [AnswerController::class, 'edit'])->name('edit_answer');
Route::put('/answer/{answer_id}', [AnswerController::class, 'update'])->name('update_answer');
Route::delete('/answer/{answer_id}', [AnswerController::class, 'destroy'])->name('delete_answer');