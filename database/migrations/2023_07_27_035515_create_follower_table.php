<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follower', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_follower');
            $table->foreign('user_follower')->references('id')->on('users');

            $table->unsignedBigInteger('user_followed');
            $table->foreign('user_followed')->references('id')->on('users');

            $table->smallInteger('status_follower');
            $table->smallInteger('status_followed');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('follower');
    }
};
