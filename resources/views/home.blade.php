@extends('layouts.master')

@section('title')
@endsection

@push('styles')
<link href="{{ asset('template/assets/libs/dropify/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{ asset('template/assets/libs/dropify/js/dropify.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('template/assets/js/pages/form-fileuploads.init.js') }}"></script>
<script>
    document.getElementById("imageContent").style.display = "none";
    document.getElementById("imageAnswer").style.display = "none";
    function myFunction() {
        var x = document.getElementById("imageContent");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
    function myFunctionAnswer() {
        var x = document.getElementById("imageAnswer");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>
@endpush
@section('content')
<div class="row">
    <div class="col-sm-8">
        @guest

        @else
        <div class="card">
            <form method="post" class="card-body" action="{{ route('store_question') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group mb-2">
                    <label>Title</label>
                    <input type="text" name="subject" id="subject" class="form-control @error('subject') is-invalid @enderror" />
                </div>
                @error('subject') 
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group mb-2">
                    <label>Content</label>
                    <textarea class="form-control tinymce-editor @error('content') is-invalid @enderror" id="content" name="content"></textarea>
                </div>
                @error('content') 
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group mb-2">
                    <label>Category</label>
                    <select id="category_id" name="category_id" class="form-select @error('category_id') is-invalid @enderror">
                        <option selected>Choose Category</option>
                        @foreach($category as $value)
                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>
                @error('category_id') 
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div id="imageContent" class="form-group">
                    <input type="file" class="@error('imageQuestion') is-invalid @enderror" name="imageQuestion" id="imageQuestion" data-plugins="dropify" data-default-file=""  />
                    <p class="text-muted text-center mt-2 mb-0">Image Content</p>
                </div>
                @error('imageQuestion') 
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group pt-1 float-end">
                    <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Send Question</button>
                </div>
                <ul class="nav nav-pills profile-pills mt-1">
                    <li>
                        <a href="#" onclick="myFunction()"><i class="fa fa-image"></i> &nbsp; Add Image</a>
                    </li>
                </ul>
            </form>
        </div>
        @endguest
        <!-- <button onclick="myFunction()">Try it</button>

        <div id="myDIV">
        This is my DIV element.
        </div> -->
        @forelse($question as $value)
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-top">
                    @if(isset($value->user->profile->image))
                    <img src="{{ asset('img/profile/'.$value->user->profile->image) }}" alt="" class="flex-shrink-0 comment-avatar avatar-sm rounded me-2">
                    @endif
                    <div class="flex-grow-1">
                        <h5 class="mt-0"><a href="#" class="text-dark">{{$value->user->name}}</a></h5>
                    </div>
                </div>
                <h5 class="mt-2"><a href="#" class="text-dark">{{$value->subject}}</a></h5>
                <p class="card-text">{{ Str::limit($value->content, 50)}}</p>
            </div>
            @if($value->image != null)
            <img height="300px" src="{{ asset('img/question/'.$value->image) }}" alt="Card image cap">
            @endif
            <div class="card-body">
                <!-- <a href="#" class="card-link">Card link</a>
                <a href="#" class="card-link">Another link</a> -->
                <div class="comment-footer">
                    <a onclick="" data-bs-toggle="modal" data-bs-target="#answer-modal{{ $value->id }}">Reply</a>
                </div>

                <!-- <div class="d-flex align-items-top mb-2 mt-3">
                    <img src="{{ asset('template/assets/images/users/user-3.jpg') }}" alt="" class="flex-shrink-0 comment-avatar avatar-sm rounded me-2">
                    <div class="flex-grow-1">
                        <h5 class="mt-0"><a href="#" class="text-dark">John Smith</a><small class="ms-1 text-muted">about 1 hour ago</small></h5>
                        <p>Wow impressive!</p>

                        <div class="comment-footer">
                            <a href="#"><i class="far fa-thumbs-up"></i></a>
                            <a href="#"><i class="far fa-thumbs-down"></i></a>
                            <a href="#">Reply</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-top">
                    <img src="{{ asset('template/assets/images/users/user-4.jpg') }}" alt="" class="flex-shrink-0 comment-avatar avatar-sm rounded me-2">
                    <div class="flex-grow-1">
                        <h5 class="mt-0"><a href="#" class="text-dark">Matt Cheuvront</a><small class="ms-1 text-muted">about 2 hour ago</small></h5>
                        <p>Wow, that is really nice.</p>

                        <div class="comment-footer mb-3">
                            <a href="#"><i class="far fa-thumbs-up"></i></a>
                            <a href="#"><i class="far fa-thumbs-down"></i></a>
                            <a href="#">Reply</a>
                        </div>

                        <div class="d-flex align-items-top mb-2">
                            <img src="{{ asset('template/assets/images/users/user-5.jpg') }}" alt="" class="flex-shrink-0 comment-avatar avatar-sm rounded me-2">
                            <div class="flex-grow-1">
                                <h5 class="mt-0"><a href="#" class="text-dark">Stephanie Walter</a><small class="ms-1 text-muted">about 3 hour ago</small></h5>
                                <p>Nice work, makes me think of The Money Pit.</p>

                                <div class="comment-footer">
                                    <a href="#"><i class="far fa-thumbs-up"></i></a>
                                    <a href="#"><i class="far fa-thumbs-down"></i></a>
                                    <a href="#">Reply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div id="answer-modal{{ $value->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{ route('store_answer') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="text" name="question_id" id="question_id" value="{{ $value->id }}" class="form-control @error('question_id') is-invalid @enderror" hidden/>
                        <div class="modal-header">
                            <h4 class="modal-title">Reply</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-2">
                                        <textarea class="form-control" id="reply" name="reply" placeholder="Reply"></textarea>
                                    </div>
                                    <div id="imageAnswer" class="form-group">
                                        <input type="file" class="@error('image_answer') is-invalid @enderror" name="image_answer" id="image_answer" data-plugins="dropify" data-default-file=""  />
                                        <p class="text-muted text-center mt-2 mb-0">Image</p>
                                    </div>
                                    @error('image_answer') 
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <ul class="nav nav-pills profile-pills mt-1">
                                <li>
                                    <a href="#" onclick="myFunctionAnswer()"><i class="fa fa-image"></i> &nbsp; Add Image</a>
                                </li>
                            </ul>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info waves-effect waves-light">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @empty
        <div class="card">
            <div class="card-body">
                <p class="card-text text-center">Belum Ada Pertanyaan</p>
            </div>
        </div>
        @endforelse
    </div>
    @guest

    @else
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <div class="dropdown float-end">
                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-dots-vertical"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Another action</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Something else</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Separated link</a>
                    </div>
                </div>

                <h4 class="header-title mt-0 mb-3">My Followers</h4>

                <ul class="list-group mb-0 user-list">
                    @if($follower != null)
                        @foreach($follower as $fol)
                            <li class="list-group-item">
                                <a href="#" class="user-list-item">
                                    <div class="user avatar-sm float-start me-2">
                                        <img src="{{ asset('img/profile/'.$fol->user_follower->profile->image) }}" alt="" class="img-fluid rounded-circle">
                                    </div>
                                    <div class="user-desc">
                                        <h5 class="name mt-0 mb-1">{{ $fol->user_follower->name }}</h5>
                                        <p class="desc text-muted mb-0 font-12">{{ $fol->user_follower>profile->biodata }}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @endguest
</div>
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  
<script type="text/javascript">
        tinymce.init({
        selector: 'textarea.tinymce-editor',
        height: 300,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount', 'image'
        ],
        toolbar: 'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
        content_css: '//www.tiny.cloud/css/codepen.min.css'
    });
</script>
@endpush