@extends('layouts.master')

@section('title')
@endsection

@push('styles')
<link href="{{ asset('template/assets/libs/dropify/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script src="{{ asset('template/assets/libs/dropify/js/dropify.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('template/assets/js/pages/form-fileuploads.init.js') }}"></script>
<script>
    document.getElementById("editProfile").style.display = "none";
    function editProfile() {
        var x = document.getElementById("detailProfile");
        var y = document.getElementById("editProfile");
        if (y.style.display === "none") {
            x.style.display = "none";
            y.style.display = "block";
        } else {
            x.style.display = "block";
            y.style.display = "none";
        }
    }
</script>
@endpush

@section('content')
<div class="row">
    <div class="col-sm-8">
        <div class="card">
            @if($detailProfile==null)
            <div class="bg-picture card-body">
                <div class="d-flex align-items-top">
                    <img src="{{ asset('template/assets/images/users/user_default.png') }}"
                            class="flex-shrink-0 rounded-circle avatar-xl img-thumbnail float-start me-3" alt="profile-image">   
                    <div id="addProfile">
                        <h4 class="m-0 text-capitalize">{{ $user->name }}</h4>
                        <p class="text-muted"><i>{{ $user->email }}</i></p>
                        <form method="post" class="card-body" action="{{ route('store_profile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mb-2">
                                <label>Address</label>
                                <textarea name="address" id="address" class="form-control tinymce-editor @error('address') is-invalid @enderror"></textarea>
                            </div>
                            @error('address') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group mb-2">
                                <label>Biodata</label>
                                <textarea class="form-control tinymce-editor @error('biodata') is-invalid @enderror" id="biodata" name="biodata"></textarea>
                            </div>
                            @error('biodata') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group mb-2">
                                <label>Birth</label>
                                <input type="date" name="birth" id="birth" class="form-control tinymce-editor @error('birth') is-invalid @enderror"/>
                            </div>
                            @error('birth') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div id="imageProfile" class="form-group">
                                <label>Image Profile</label>

                                <input type="file" class="@error('imageProfile') is-invalid @enderror" name="imageProfile" id="imageProfile" data-plugins="dropify" data-default-file=""  />
                            </div>
                            @error('imageProfile') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group pt-1 float-end">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @else
            <div class="bg-picture card-body">
                <div class="dropdown float-end">
                    <a type="button" type="button" class="btn width-lg btn-primary rounded-pill waves-effect waves-light" onclick="editProfile()">Edit Profile</a>
                </div>
                <div class="d-flex align-items-top">
                    <img src="{{ asset('img/profile/'.$detailProfile->image) }}"
                            class="flex-shrink-0 rounded-circle avatar-xl img-thumbnail float-start me-3" alt="profile-image">
                    <div id="detailProfile" class="flex-grow-1 overflow-hidden">
                        <h4 class="m-0 text-capitalize">{{ $detailProfile->user->name }}</h4>
                        <p class="text-muted"><i>{{ $detailProfile->user->email }}</i></p>
                        <p class="font-13">{{ $detailProfile->address }}</p>
                        <p class="font-13">{{ $detailProfile->biodata }}</p>
                        <p class="font-13">{{ $detailProfile->age }}</p>
                        <p class="font-13">{{ $detailProfile->birth }}</p>
                        <ul class="social-list list-inline mt-3 mb-0">
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-purple text-purple"><i
                                        class="mdi mdi-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                        class="mdi mdi-google"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                        class="mdi mdi-twitter"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i
                                        class="mdi mdi-github"></i></a>
                            </li>
                        </ul>
                        
                    </div>
                    <div id="editProfile">
                        <form method="post" class="card-body" action="{{ route('store_profile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mb-2">
                                <label>Address</label>
                                <textarea name="address" id="address" class="form-control tinymce-editor @error('address') is-invalid @enderror">{{ $detailProfile->address }}</textarea>
                            </div>
                            @error('address') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group mb-2">
                                <label>Biodata</label>
                                <textarea class="form-control tinymce-editor @error('biodata') is-invalid @enderror" id="biodata" name="biodata">{{ $detailProfile->biodata }}</textarea>
                            </div>
                            @error('biodata') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group mb-2">
                                <label>Birth</label>
                                <input type="date" name="birth" id="birth" value="{{ $detailProfile->birth }}" class="form-control tinymce-editor @error('birth') is-invalid @enderror"/>
                            </div>
                            @error('birth') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div id="imageProfile" class="form-group">
                                <label>Image Profile</label>

                                <input type="file" class="@error('imageProfile') is-invalid @enderror" name="imageProfile" id="imageProfile" data-plugins="dropify" data-default-file="{{ asset('img/profile/'.$detailProfile->image) }}"  />
                            </div>
                            @error('imageProfile') 
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group pt-1 float-end">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Send</button>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @endif
            <div class="card-body">
                <div class="align-items-top ps-5">
                    <h4 class="m-0 text-capitalize">Change Password</h4>
                    <form method="post" class="card-body" action="{{ route('change_password') }}" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group mb-2">
                            <label>Current Password</label>
                            <input type="password" name="current_password" id="current_password" class="form-control tinymce-editor @error('current_password') is-invalid @enderror"/>
                        </div>
                        @error('current_password') 
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mb-2">
                            <label>New Password</label>
                            <input type="password" name="new_password" id="new_password" class="form-control tinymce-editor @error('new_password') is-invalid @enderror"/>
                        </div>
                        @error('new_password') 
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group mb-2">
                            <label>Confirm New Password</label>
                            <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control tinymce-editor @error('confirm_new_password') is-invalid @enderror"/>
                        </div>
                        @error('confirm_new_password') 
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group pt-1 float-end">
                            <button type="submit" class="btn btn-primary btn-sm waves-effect waves-light">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--/ meta -->

    </div>

    @guest

    @else
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
                <div class="dropdown float-end">
                    <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="mdi mdi-dots-vertical"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Action</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Another action</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Something else</a>
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item">Separated link</a>
                    </div>
                </div>

                <h4 class="header-title mt-0 mb-3">My Followers</h4>

                <ul class="list-group mb-0 user-list">
                    @if($follower != null)
                        @foreach($follower as $fol)
                            <li class="list-group-item">
                                <a href="#" class="user-list-item">
                                    <div class="user avatar-sm float-start me-2">
                                        <img src="{{ asset('img/profile/'.$fol->user_follower->profile->image) }}" alt="" class="img-fluid rounded-circle">
                                    </div>
                                    <div class="user-desc">
                                        <h5 class="name mt-0 mb-1">{{ $fol->user_follower->name }}</h5>
                                        <p class="desc text-muted mb-0 font-12">{{ $fol->user_follower>profile->biodata }}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
    @endguest
</div> 
@endsection